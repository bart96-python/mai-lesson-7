from urllib import parse, request
from math import ceil
import json


def getUrl(page):
	protocol = 'http'
	hostName = 'openapi.clearspending.ru'
	pathName = 'restapi/v3/contracts/select'
	search = {
		'supplierinn': '7712038455',
		'sort': '-price',
		'page': page,
	}

	return f'{protocol}://{hostName}/{pathName}/?{parse.urlencode(search)}'


def showResults(contracts):
	for index in range(10):
		print(f'ТОП 10 по цене: {index + 1}.\t {contracts[index]["price"]}')

	print('\n\n')

	for index, contract in enumerate(contracts):
		if contract['products'][0].get('OKDP'):
			products = contract['products'][0]['OKDP']['code']
		else:
			products = '-'

		customer = contract['customer']['inn']

		if contract.get('finances') and contract['finances'].get('budget'):
			finances = contract['finances']['budget']['code']
		else:
			finances = '-'

		print(f'{index + 1}.\t ПродуктКод: {products}, Заказчик: {customer}, БюджетКод: {finances}')


def main():
	page = 0 # PROD
	page = 8 # DEV

	contracts = []

	while True:
		page = page + 1
		res = request.urlopen(getUrl(page))

		encoding = res.info().get_content_charset('utf-8')
		dataJson = json.loads(res.read().decode(encoding))

		contracts = contracts + dataJson['contracts']['data']

		page = dataJson['contracts']['page']
		total = dataJson['contracts']['total']
		perpage = dataJson['contracts']['perpage']

		print(f'Просканировано: {ceil(page * 100 / ceil(total / perpage))}%')

		if ceil(total / perpage) <= page:
			showResults(contracts)
			break


if __name__ == "__main__":
	main()
