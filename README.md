# MAI Lesson 7 – API ClearSpending.ru
## Запросы на сайты и парсинг полученных данных

#### HomeWork
1. Используя api ресурса [ClearSpending.ru](https://clearspending.ru), необходимо получить:
	- топ-10 контрактов, где МАИ был исполнителем, по каждому году из выборки (получается как минимум за 10 лет);
	- по каждому контракту:
		- предмет закупки (код ОКПД2 с расшифровкой); 
		- заказчик;
		- бюджет.

#### Helpful
- [Документация по API ClearSpending](https://github.com/clearspending/clearspending-examples/wiki)
- [Документация по API ClearSpending – Выборка по контрактам (select)](https://github.com/datacoon/clearspending-examples/wiki/Описание-API-Контракты#Выборка-по-контрактам-select)

#### Info
Первоначальная ссылка будет вида `http://openapi.clearspending.ru/restapi/v3/contracts/select/?supplierinn=7712038455&page=1`, не забывайте:
- нужен `select`, а не `search`;
- про пагинацию (прописана прямо в ссылке для простоты)